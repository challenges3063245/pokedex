//
//  PokemonType+CoreDataClass.swift
//  
//
//  Created by Ariel Díaz on 19/01/24.
//
//

import Foundation
import CoreData

@objc(PokemonType)
public class PokemonType: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "PokemonType", in: context) else {
                fatalError("Failed to decode PokemonType")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.slot = try container.decodeIfPresent(Int64.self, forKey: .slot) ?? 0
        self.type = try container.decodeIfPresent(GenericObject.self, forKey: .type)
    }

}

extension PokemonType: Decodable {

    enum CodingKeys: String, CodingKey {
        case slot
        case type
    }

}
