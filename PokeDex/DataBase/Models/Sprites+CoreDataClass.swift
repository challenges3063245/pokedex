//
//  Sprites+CoreDataClass.swift
//  
//
//  Created by Ariel Díaz on 18/01/24.
//
//

import Foundation
import CoreData

@objc(Sprites)
public class Sprites: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Sprites", in: context) else {
                fatalError("Failed to decode Sprites")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.backDefault = (try? container.decodeIfPresent(String.self, forKey: .backDefault)) ?? ""
        self.backShiny = (try? container.decodeIfPresent(String.self, forKey: .backShiny)) ?? ""
        self.frontDefault = (try? container.decodeIfPresent(String.self, forKey: .frontDefault)) ?? ""
        self.frontShiny = (try? container.decodeIfPresent(String.self, forKey: .frontShiny)) ?? ""
        self.other = try? container.decodeIfPresent(OtherSprites.self, forKey: .other)
    }

}

extension Sprites: Decodable {

    enum CodingKeys: String, CodingKey {
        case backDefault = "back_default"
        case backShiny = "back_shiny"
        case frontDefault = "front_default"
        case frontShiny = "front_shiny"
        case other
    }

}

public enum SpriteType: String {
    case svg, png
}
