//
//  Pokemon+CoreDataClass.swift
//  
//
//  Created by Ariel Díaz on 18/01/24.
//
//

import Foundation
import CoreData

@objc(Pokemon)
public class Pokemon: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Pokemon", in: context) else {
                fatalError("Failed to decode Pokemon")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decodeIfPresent(Int64.self, forKey: .id) ?? 0
        self.order = try container.decodeIfPresent(Int64.self, forKey: .order) ?? 0
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.species = try container.decodeIfPresent(GenericObject.self, forKey: .species)
        self.sprites = try container.decodeIfPresent(Sprites.self, forKey: .sprites)
        self.types = NSSet(array: try container.decodeIfPresent([PokemonType].self, forKey: .types) ?? [])
    }

}

extension Pokemon: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case order
        case name
        case sprites
        case species
        case types
    }

    var pokemonTypes: [String] {
        (types?.allObjects as? [PokemonType])?
            .sorted(by: { $0.slot < $1.slot })
            .map { $0.type?.name ?? "" }
        ?? []
    }

}
