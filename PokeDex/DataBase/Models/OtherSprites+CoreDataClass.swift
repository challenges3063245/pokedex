//
//  OtherSprites+CoreDataClass.swift
//  
//
//  Created by Ariel Díaz on 26/01/24.
//
//

import Foundation
import CoreData

@objc(OtherSprites)
public class OtherSprites: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "OtherSprites", in: context) else {
                fatalError("Failed to decode OtherSprites")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.dreamWorld = try container.decodeIfPresent(Sprites.self, forKey: .dreamWorld)
    }

}

extension OtherSprites: Decodable {

    enum CodingKeys: String, CodingKey {
        case dreamWorld = "dream_world"
    }

}
