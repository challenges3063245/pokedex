//
//  ColorCatalog+CoreDataProperties.swift
//  
//
//  Created by Ariel Díaz on 25/01/24.
//
//

import Foundation
import CoreData


extension ColorCatalog {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ColorCatalog> {
        return NSFetchRequest<ColorCatalog>(entityName: "ColorCatalog")
    }

    @NSManaged public var results: NSSet?

}

// MARK: Generated accessors for results
extension ColorCatalog {

    @objc(addResultsObject:)
    @NSManaged public func addToResults(_ value: GenericObject)

    @objc(removeResultsObject:)
    @NSManaged public func removeFromResults(_ value: GenericObject)

    @objc(addResults:)
    @NSManaged public func addToResults(_ values: NSSet)

    @objc(removeResults:)
    @NSManaged public func removeFromResults(_ values: NSSet)

}
