//
//  Sprites+CoreDataProperties.swift
//  
//
//  Created by Ariel Díaz on 18/01/24.
//
//

import Foundation
import CoreData


extension Sprites {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Sprites> {
        return NSFetchRequest<Sprites>(entityName: "Sprites")
    }

    @NSManaged public var backDefault: String?
    @NSManaged public var backShiny: String?
    @NSManaged public var frontDefault: String?
    @NSManaged public var frontShiny: String?
    @NSManaged public var pokemon: Pokemon?
    @NSManaged public var other: OtherSprites?

}
