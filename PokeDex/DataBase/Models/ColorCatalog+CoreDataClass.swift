//
//  ColorCatalog+CoreDataClass.swift
//  
//
//  Created by Ariel Díaz on 25/01/24.
//
//

import Foundation
import CoreData

@objc(ColorCatalog)
public class ColorCatalog: NSManagedObject {

    public required convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "ColorCatalog", in: context) else {
            throw NSError(domain: "Failed to decode ColorCatalog", code: 44)
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        let results = (try? container.decodeIfPresent([GenericObject].self, forKey: .results)) ?? []
        self.results = NSSet(array: results)
    }

    convenience init(with colors: [GenericObject]) {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "ColorCatalog", in: context) else {
            fatalError("Failed to get ColorCatalog Entity")
        }
        self.init(entity: entity, insertInto: context)
        self.results = NSSet(array: colors)
    }

}

extension ColorCatalog: Decodable {

    enum CodingKeys: String, CodingKey {
        case results
    }

}
