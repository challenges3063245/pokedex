//
//  Pokemon+CoreDataClass.swift
//  
//
//  Created by Ariel Díaz on 17/01/24.
//
//

import Foundation
import CoreData

@objc(GenericObject)
public class GenericObject: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "GenericObject", in: context) else {
                fatalError("Failed to decode GenericObject")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.uuid = UUID()
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
    }

}

extension GenericObject: Decodable {

    enum CodingKeys: String, CodingKey {
        case name
        case url
    }

}
