//
//  PokemonType+CoreDataProperties.swift
//  
//
//  Created by Ariel Díaz on 19/01/24.
//
//

import Foundation
import CoreData


extension PokemonType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PokemonType> {
        return NSFetchRequest<PokemonType>(entityName: "PokemonType")
    }

    @NSManaged public var slot: Int64
    @NSManaged public var type: GenericObject?
    @NSManaged public var pokemon: Pokemon?

}
