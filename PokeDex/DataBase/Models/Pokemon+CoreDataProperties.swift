//
//  PokemonForm+CoreDataProperties.swift
//  
//
//  Created by Ariel Díaz on 18/01/24.
//
//

import Foundation
import CoreData


extension Pokemon {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pokemon> {
        return NSFetchRequest<Pokemon>(entityName: "Pokemon")
    }

    @NSManaged public var id: Int64
    @NSManaged public var order: Int64
    @NSManaged public var name: String?
    @NSManaged public var species: GenericObject?
    @NSManaged public var sprites: Sprites?
    @NSManaged public var types: NSSet?

}

extension Pokemon {

    @objc(addTypesObject:)
    @NSManaged public func addToTypes(_ value: PokemonType)

    @objc(removeTypesObject:)
    @NSManaged public func removeFromTypes(_ value: PokemonType)

    @objc(addTypes:)
    @NSManaged public func addToTypes(_ values: NSSet)

    @objc(removeTypes:)
    @NSManaged public func removeFromTypes(_ values: NSSet)

}
