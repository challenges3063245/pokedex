//
//  Pokemon+CoreDataProperties.swift
//  
//
//  Created by Ariel Díaz on 17/01/24.
//
//

import Foundation
import CoreData


extension GenericObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GenericObject> {
        return NSFetchRequest<GenericObject>(entityName: "GenericObject")
    }

    @NSManaged public var name: String?
    @NSManaged public var url: String?
    @NSManaged public var uuid: UUID?

}
