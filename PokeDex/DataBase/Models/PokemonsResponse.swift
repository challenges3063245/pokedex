//
//  PokemonsResponse.swift
//  PokeDex
//
//  Created by Ariel Díaz on 17/01/24.
//

import Foundation

class PokemonsResponse<T: Decodable> {

    var count: Int
    var next: String?
    var previous: String?
    var results: [T]

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.count = try container.decode(Int.self, forKey: .count)
        self.next = try container.decode(String?.self, forKey: .next)
        self.previous = try container.decode(String?.self, forKey: .previous)
        self.results = try container.decode([T].self, forKey: .results)
    }

}

extension PokemonsResponse: Decodable {

    enum CodingKeys: String, CodingKey {
        case count
        case next
        case previous
        case results
    }

}



struct PokemonStruct: Codable {
    var id: Int64
    var order: Int64
    var name: String
    var species: GenericStruct?
    var sprites: SpritesStruct?
    var types: [PokemonTypeStruct]
}

struct GenericStruct: Codable {
    var name: String
    var url: String
}

struct SpritesStruct: Codable {
    var backDefault: String?
    var backShiny: String?
    var frontDefault: String?
    var frontShiny: String?
}

struct PokemonTypeStruct: Codable {
    var slot: Int64
    var type: GenericStruct
}
