//
//  OtherSprites+CoreDataProperties.swift
//  
//
//  Created by Ariel Díaz on 26/01/24.
//
//

import Foundation
import CoreData


extension OtherSprites {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OtherSprites> {
        return NSFetchRequest<OtherSprites>(entityName: "OtherSprites")
    }

    @NSManaged public var dreamWorld: Sprites?

}
