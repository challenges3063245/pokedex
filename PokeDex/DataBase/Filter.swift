//
//  Filter.swift
//  PokeDex
//
//  Created by Ariel Díaz on 23/01/24.
//

import Foundation

struct Filter {
    var querySnippet: String
    var arguments: [Any]
}
