//
//  ContentView.swift
//  PokeDex
//
//  Created by Ariel Díaz on 14/01/24.
//

import SwiftUI
import CoreData
import DesignSystem

struct ContentView: View {

    let builder: HomeBuilder

    init() {
        self.builder = HomeBuilder()
        self.setupNavigationBar()
    }

    var body: some View {
        builder.build()
    }

    private func setupNavigationBar() {
        let appearance = UINavigationBar.appearance()
        appearance.titleTextAttributes = [.font: UIFont.freeMediumUp!]
        appearance.largeTitleTextAttributes = [.font: UIFont.freeTitle!]
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor : UIColor.label,
            NSAttributedString.Key.font : UIFont.sevenSegmentMediumDown as Any
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }

}

#Preview {
    ContentView()
}
