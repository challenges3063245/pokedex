//
//  PokeDexApp.swift
//  PokeDex
//
//  Created by Ariel Díaz on 14/01/24.
//

import SwiftUI

@main
struct PokeDexApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, CoreDataManager.shared.context)
        }
    }
}
