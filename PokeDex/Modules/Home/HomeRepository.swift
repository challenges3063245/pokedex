//
//  HomeRepository.swift
//  PokeDex
//
//  Created by Ariel Díaz on 17/01/24.
//

import Foundation
import Combine
import Network

class HomeRepository {

    enum Paths: String {
        case pokemon = "/pokemon"
        case colors = "/pokemon-color"
    }

    private let baseUrl: String
    private let decoder:JSONDecoder
    private let client: NetworkAPI

    init(client: NetworkAPI = NetworkAPI.shared) {
        self.baseUrl = "https://pokeapi.co/api/v2"
        self.decoder = JSONDecoder()
        self.client = client
    }

    func getPokemons() -> AnyPublisher<[GenericObject], Error> {
        let urlString: String = baseUrl + Paths.pokemon.rawValue + "?limit=100000&offset=0"
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        return client.getSerializedData(with: request)
            .tryMap { (response: PokemonsResponse<GenericObject>) in
                return response.results
            }
            .eraseToAnyPublisher()
    }

    func getPokemon(idOrName: String) -> AnyPublisher<Pokemon, Error> {
        let urlString: String = baseUrl + Paths.pokemon.rawValue + "/\(idOrName)"
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        return client.getSerializedData(with: request)
            .eraseToAnyPublisher()
    }

    func getColorCatalog() -> AnyPublisher<ColorCatalog, Error> {
        let url: URL = URL(string: baseUrl + Paths.colors.rawValue)!
        let request = URLRequest(url: url)
        return client.getSerializedData(with: request)
            .tryMap { (response: PokemonsResponse<GenericObject>) in
                let catalog = ColorCatalog(with: response.results)
                return catalog
            }
            .eraseToAnyPublisher()
    }

    func getColor(idOrName: String) {
//        let urlString: String = baseUrl + Paths.colors.rawValue + "/\(idOrName)"
//        let url = URL(string: urlString)!
//        let request = URLRequest(url: url)
        
    }

}
