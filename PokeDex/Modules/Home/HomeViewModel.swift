//
//  HomeViewModel.swift
//  PokeDex
//
//  Created by Ariel Díaz on 17/01/24.
//

import Foundation
import Combine
import DesignSystem

class HomeViewModel {

    private var useCase: HomeUseCase

    var subscriptions = Set<AnyCancellable>()

    init(useCase: HomeUseCase) {
        self.useCase = useCase
    }

    func bind(_ input: HomeViewModelInput) {
        input.$searchText
//            .debounce(for: .seconds(0.5), scheduler: RunLoop.main)
            .sink { completion in
                debugPrint(completion)
            }
            .store(in: &subscriptions)

        input.$onAppear
            .sink(receiveValue: didLoadHandler)
            .store(in: &subscriptions)
    }

    private lazy var didLoadHandler: (@escaping Completion) -> Void = { [weak self] _ in
        debugPrint("[📱]: HomeViewModel.didLoadHandler")
        guard let self else { return }
        self.useCase.getPokemons().sink { [weak self] completion in
            debugPrint("[📱]: HomeViewModel.didLoadHandler.completion")
            if case .failure(let error) = completion {
                debugPrint(error)
                if DataSource.emptyError as NSError == error as NSError {
                    self?.getPokemons()
                }
            }
        } receiveValue: { (pokemons: [Pokemon]) in
            debugPrint("[📱]: HomeViewModel.didLoadHandler.pokemonForms")
        }.store(in: &self.subscriptions)
    }

    private func getPokemons() {
        debugPrint("[📱]: HomeViewModel.getPokemons")
        self.useCase.getPokemons().sink { completion in
            if case .failure(let error) = completion {
                debugPrint(error)
            }
        } receiveValue: { [weak self] (pokemons: [GenericObject]) in
            guard let self = self else { return }
            pokemons.forEach {
                self.getPokemon(idOrName: $0.name ?? "")
            }
        }.store(in: &subscriptions)
    }

    private func getPokemon(idOrName: String) {
        debugPrint("[📱]: HomeViewModel.getPokemonForm with idOrName \(idOrName)")
        self.useCase.getPokemon(idOrName: idOrName).sink { completion in
            if case .failure(let error) = completion {
                debugPrint(error)
            }
        } receiveValue: { pokemonForm in
            debugPrint(pokemonForm)
            DataSource.shared.saveContext()
        }
        .store(in: &subscriptions)
    }
}
