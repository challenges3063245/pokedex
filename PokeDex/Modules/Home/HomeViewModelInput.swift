//
//  HomeViewModelInput.swift
//  PokeDex
//
//  Created by Ariel Díaz on 17/01/24.
//

import Foundation
import Combine

class HomeViewModelInput: ObservableObject {

    public static var shared: HomeViewModelInput = HomeViewModelInput()

    @Published var searchText: String = ""
    @Published var isSearching: Bool = false
    @Published var inverseFilter: Bool = false
    @Published var fetchLimit: Int = 10

    @Published var onAppear: () -> Void = { }

    var conditionForId: Bool {
        isSearching && searchText != ""
    }

    var filters: [Filter] {
        [
            .init(querySnippet: "id <= %@ ", arguments: [conditionForId ? 0 : fetchLimit]),
            .init(querySnippet: "OR id <= %@ ", arguments: [Int(searchText) ?? 0]),
            .init(querySnippet: "OR %K CONTAINS[cd] %@ ", arguments: [#keyPath(Pokemon.name), searchText]),
            .init(querySnippet: "OR ANY types.type.name CONTAINS[cd] %@", arguments: [searchText]),
        ]
    }

    var sortDescriptors: [NSSortDescriptor] {
        [
            NSSortDescriptor(key: "id", ascending: !inverseFilter)
        ]
    }

    private init() { }

}
