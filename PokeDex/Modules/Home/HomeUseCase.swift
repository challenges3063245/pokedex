//
//  HomeUseCase.swift
//  PokeDex
//
//  Created by Ariel Díaz on 18/01/24.
//

import Foundation
import Combine

class HomeUseCase {

    let repository: HomeRepository
    let dbClient: DataSource

    init(repository: HomeRepository, dbClient: DataSource = .shared) {
        self.repository = repository
        self.dbClient = dbClient
    }


    // MARK: Local data
    func getPokemons() -> AnyPublisher<[Pokemon], Error> {
        let sortDescriptor = NSSortDescriptor(key: "order", ascending: true)
        return self.dbClient.fetchObjects(sort: [sortDescriptor]).eraseToAnyPublisher()
    }

//    func getPokemonForm(with name: String)  -> AnyPublisher<PokemonForm, Error> {
//        let forms: [PokemonForm] = 
//        return
//    }

    // MARK: Remote data
    func getPokemons() -> AnyPublisher<[GenericObject], Error> {
        return self.repository.getPokemons().eraseToAnyPublisher()
    }

    func getPokemon(idOrName: String) -> AnyPublisher<Pokemon, Error> {
        return self.repository.getPokemon(idOrName: idOrName).eraseToAnyPublisher()
    }

    func getColorCatalog() -> AnyPublisher<ColorCatalog, Error> {
        return self.repository.getColorCatalog().eraseToAnyPublisher()
    }

}
