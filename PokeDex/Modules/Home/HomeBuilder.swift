//
//  HomeBuilder.swift
//  PokeDex
//
//  Created by Ariel Díaz on 19/01/24.
//

import SwiftUI

struct HomeBuilder {

    func build() -> some View {
        let repository = HomeRepository()
        let useCase = HomeUseCase(repository: repository)
        let viewModel = HomeViewModel(useCase: useCase)
        return Home(viewModel: viewModel, input: .shared)
    }

}
