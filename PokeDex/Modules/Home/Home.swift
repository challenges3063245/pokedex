//
//  Home.swift
//  PokeDex
//
//  Created by Ariel Díaz on 31/01/24.
//

import SwiftUI
import DesignSystem

let animationTime: CGFloat = 0.3

enum OverlayPosition: String {
    case home, detail
}

struct Home: View {

    private var viewModel: HomeViewModel

    @ObservedObject var input: HomeViewModelInput

    @State var itemSelected: ItemViewData?
    @State var overlay: OverlayPosition = .home
    @State var detailView: Bool = false

    init(viewModel: HomeViewModel, input: HomeViewModelInput) {
        self.viewModel = viewModel
        self.input = input
        self.bind()
    }

    var body: some View {
        NavigationStack {
            scrollableContent
        }
        .background {
            GeometryView()
        }
        .overlayPreferenceValue(MAnchorKey.self, detailView(_:))
    }

    @ViewBuilder
    private var scrollableContent: some View {
        ScrollView {
            GridView(input: input) { (item: ItemViewData) in
                Button {
                    if itemSelected == nil {
                        withAnimation {
                            hideKeyboard()
                            itemSelected = item
                            detailView.toggle()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                                overlay = .detail
                            }
                        }
                    }
                } label: {
                    PokemonItemView(viewData: item)
                        .padding(EdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4))
                }
                .anchorPreference(key: MAnchorKey.self, value: .bounds, transform: {
                    ["\(item.name)": $0]
                })
            }
        }
        .scrollDisabled(detailView)
        .searchable(
            text: $input.searchText,
            isPresented: $input.isSearching,
            placement: .navigationBarDrawer(displayMode: .always),
            prompt: Text("Search by type, weakness, ability and more")
        )
        .navigationBarTitle("Pokedex", displayMode: .large)
        .environment(\.font, .sevenSegmentMediumDown)
        .scrollDismissesKeyboard(.interactively)
        .onAppear(perform: input.onAppear)
    }

    @ViewBuilder
    private func detailView(_ value: [String: Anchor<CGRect>]) -> some View {
        GeometryReader { geometry in
            if let itemSelected {
                let key: String = overlay == .detail ? overlay.rawValue : "\(itemSelected.name)"
                if let anchor = value[key] {
                    let rect = geometry[anchor]
                    DetailView(viewData: $itemSelected, detailView: $detailView, overlay: $overlay)
                        .offset(x: rect.minX, y: rect.minY)
                        .frame(width: rect.width, height: rect.height)
                        .animation(.smooth(duration: animationTime, extraBounce: 0.1), value: rect)
                }
            }
        }
    }

    private func bind() {
        self.viewModel.bind(input)
    }

    private func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }

}
