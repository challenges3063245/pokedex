//
//  GridView.swift
//  PokeDex
//
//  Created by Ariel Díaz on 20/01/24.
//

import SwiftUI
import CoreData
import DesignSystem

struct GridView<T: Pokemon, U: Identifiable, Content: View>: View {

    @Environment(\.verticalSizeClass) var verticalSizeClass
    @Environment(\.horizontalSizeClass) var horizontalSizeClass

    let content: (U) -> Content
    weak var input: HomeViewModelInput?

    @FetchRequest private var objects: FetchedResults<T>

    private var columns: [GridItem] {
        horizontalSizeClass == .regular && verticalSizeClass == .regular
        ? Array(repeating: GridItem(.flexible()), count: 4)
        : horizontalSizeClass == .regular || verticalSizeClass == .compact
        ? Array(repeating: GridItem(.flexible()), count: 3)
        : Array(repeating: GridItem(.flexible()), count: 2)
    }

    init(input: HomeViewModelInput, @ViewBuilder content: @escaping (U) -> Content) {
        let fetchRequest: NSFetchRequest<T> = .init(entityName: String(describing: T.self))
        fetchRequest.sortDescriptors = input.sortDescriptors
        fetchRequest.fetchLimit = input.fetchLimit
        fetchRequest.predicate = NSPredicate(
            format: input.filters.map { $0.querySnippet }.reduce("", +),
            argumentArray: input.filters.map { $0.arguments }.reduce([], +)
        )
        self.input = input
        self._objects = FetchRequest<T>(fetchRequest: fetchRequest, animation: .smooth)
        self.content = content
    }

    var body: some View {
        LazyVGrid(columns: columns) {
            ForEach(objects, id: \.id) { item in
                let url = item.sprites?.frontDefault ?? ""
                let svg = item.sprites?.other?.dreamWorld?.frontDefault ?? ""
                self.content(
                    ItemViewData(
                        id: Int(item.id),
                        name: item.name ?? "",
                        types: item.pokemonTypes,
                        order: Int(item.order),
                        sprites: [.png: url, .svg: svg]
                    ) as! U
                )
                .onAppear {
                    if objects.last?.id == item.id {
                        debugPrint("[📱]: perform lazy loading task...")
                        input?.fetchLimit += 20
                    }
                }
            }
        }
    }

}
