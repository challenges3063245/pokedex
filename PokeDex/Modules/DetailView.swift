//
//  DetailView.swift
//  PokeDex
//
//  Created by Ariel Díaz on 24/01/24.
//

import SwiftUI
import Network
import CoreData
import DesignSystem
import SwiftSVG

struct DetailView: View {

    @State var data: Data?
    @State var id: UUID = UUID()
    @Binding var itemSelected: ItemViewData?
    @Binding var detailView: Bool
    @Binding var overlay: OverlayPosition

    init(viewData: Binding<ItemViewData?>, detailView: Binding<Bool>, overlay: Binding<OverlayPosition>) {
        self._itemSelected = viewData
        self._detailView = detailView
        self._overlay = overlay
    }

    var body: some View {
        if let itemSelected {
            ScrollView {
                VStack {
                    Spacer(minLength: 0)
                        .frame(height: 50)
                    Text(itemSelected.name)
                        .frame(maxWidth: .infinity)
                        .font(.sevenSegmentTitle)
                        .padding()

                    Text("\(itemSelected.id)")
                        .font(.freeTitle)

                    if canShowSVG {
                        SVGImageView(data: data!)
                            .padding(.horizontal)
                        Spacer(minLength: 0)
                            .frame(height: 550)
                    } else {
                        asyncImage
                        Spacer(minLength: 0)
                    }
                }
                .foregroundStyle(.white)
            }
            .background {
                RoundedRectangle(cornerRadius: 15).fill(
                    RadialGradient(colors: [itemSelected.colors.first ?? .black] + [.black], center: .center, startRadius: 0, endRadius: 1000)
                )
            }
            .overlay(alignment: .topTrailing) {
                if overlay == .detail {
                    closeButton
                }
            }
            .onAppear {
                if let svgUrl = itemSelected.sprites[.svg], svgUrl != "" {
                    self.loadImage(with: svgUrl)
                }
            }
        }
    }

    @ViewBuilder
    private var asyncImage: some View {
        let url = URL(string: itemSelected?.sprites[.png] ?? "")
        AsyncImage(url: url) { phase in
            if let image = phase.image {
                image
                    .resizable()
                    .scaledToFit()
            } else if phase.error != nil {
                Button {
                    id = UUID()
                } label: {
                    Image(systemName: "arrow.triangle.2.circlepath")
                        .foregroundStyle(.white)
                }
                .symbolEffect(.bounce.down, value: id)
            } else {
                ProgressView()
                    .tint(.white)
            }
        }
        .id(id)
    }

    var canShowSVG: Bool {
        guard let svg = itemSelected?.sprites[.svg], svg != "" else {
            return false
        }
        return data != nil
    }

    @ViewBuilder
    private var closeButton: some View {
        VStack {
            Spacer()
                .frame(height: 25)
            Button {
                if overlay != .home {
                    self.overlay = .home
                    withAnimation(.easeInOut) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + animationTime) {
                            self.detailView.toggle()
                            self.itemSelected = nil
                        }
                    }
                }
            } label: {
                Image(systemName: "xmark")
                    .padding(10)
                    .background(.black.opacity(0.7), in: .circle)
                    .contentShape(.circle)
                    .foregroundStyle(.white)
            }
            .padding(20)
        }
    }

    private func loadImage(with url: String) {
        NetworkAPI.shared.downloadImageData(with: url)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                if case .failure(let error) = completion {
                    debugPrint(error)
                }
            }, receiveValue: { data in
                self.data = data
            })
            .store(in: &NetworkAPI.shared.subscriptions)
    }

}
