//
//  PokemonItemView.swift
//  PokeDex
//
//  Created by Ariel Díaz on 24/01/24.
//

import SwiftUI
import Network
import Combine
import DesignSystem

public struct PokemonItemView: View {

    @State var id: UUID
    @State var viewData: ItemViewData

    public init(viewData: ItemViewData, id: UUID = UUID()) {
        self._id = State(initialValue: id)
        self._viewData = State(initialValue: viewData)
    }

    public var body: some View {
        HStack {
            leftView
                .padding(EdgeInsets(top: 0, leading: 2, bottom: 0, trailing: 2))
            rightView
                .frame(width: 70)
        }
        .background {
            RoundedRectangle(cornerRadius: 15).fill(
                RadialGradient(colors: [viewData.colors.first ?? .black] + [.black], center: .bottomTrailing, startRadius: 0, endRadius: 700)
            )
        }
    }

    @ViewBuilder
    private var typeView: some View {
        VStack {
            ForEach(0..<viewData.types.count, id: \.self) {

                let text: String = viewData.types[$0]
                HStack {
                    HStack {
                        if let color = viewData.colors.first {
                            Image(systemName: "parkingsign.circle.fill")
                                .foregroundStyle(color)
                        }

                        Text(text)
                            .foregroundStyle(.white)
                            .padding(EdgeInsets(top: 0, leading: -4, bottom: 0, trailing: 4))
                    }
                    .background {
                        RoundedRectangle(cornerRadius: 15)
                            .fill(Color.gray.opacity(0.8))
                            .blur(radius: 0.5)
                    }
                    .padding(.leading)
                    Spacer()
                }
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 20)
                .frame(alignment: .leading)

            }
        }
    }

    @ViewBuilder
    private var leftView: some View {
        VStack(alignment: .leading) {
            Text(viewData.name)
                .padding(EdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8))
                .foregroundColor(.white)
            Spacer()
            typeView
            Spacer()
        }
        .font(.sevenSegmentSmallUp)
    }

    @ViewBuilder
    private var rightView: some View {
        VStack(alignment: .trailing) {
            Text("\(viewData.id)")
                .minimumScaleFactor(0.5)
                .lineLimit(1)
                .foregroundStyle(.white)
                .font(.freeSmallUp)
                .padding()
            Spacer()
            asyncImage
                .frame(width: 70, height: 80)
        }
    }

    @ViewBuilder
    private var asyncImage: some View {
        let url = URL(string: viewData.sprites[.png] ?? "")
        AsyncImage(url: url) { phase in
            if let image = phase.image {
                image
                    .resizable()
                    .scaledToFit()
            } else if phase.error != nil {
                Button {
                    id = UUID()
                } label: {
                    Image(systemName: "arrow.triangle.2.circlepath")
                        .foregroundStyle(.white)
                }
                .symbolEffect(.bounce.down, value: id)
            } else {
                ProgressView()
                    .tint(.white)
            }
        }
        .id(id)
    }

}
