//
//  ItemViewData.swift
//  PokeDex
//
//  Created by Ariel Díaz on 24/01/24.
//

import Foundation
import SwiftUI

public struct ItemViewData: Identifiable {

    public var id: Int
    var name: String
    var types: [String]
    var weakness: String
    var order: Int
    var sprites: [SpriteType: String]

    var filters: [Filter] {
        [
            .init(querySnippet: "id == %@ ", arguments: [id]),
            .init(querySnippet: "OR name == %@", arguments: [name])
        ]
    }

    var colors: [Color] {
        types.map {
            PokemonTypes(rawValue: $0)?.color ?? .blue
        }
    }

    public init(id: Int = 0, name: String = "", types: [String] = [], weakness: String = "", order: Int = 0, sprites: [SpriteType: String]) {
        self.name = name
        self.types = types
        self.weakness = weakness
        self.id = id
        self.order = order
        self.sprites = sprites
    }

}

enum PokemonTypes: String {
    case electric
    case water
    case fire
    case poison
    case ground
    case flying
    case normal
    case grass
    case bug
    case psychic
    case dark
    case ice
    case fairy
    case dragon
    case rock
    case ghost
    case fighting
    case steel

    var color: Color {
        switch self {
        case .electric: return .yellow
        case .water:    return .blue
        case .fire:     return .red
        case .poison:   return .purple
        case .ground:   return .brown
        case .flying:   return .gray
        case .normal:   return .orange
        case .grass:    return .green
        case .bug:      return .green
        case .psychic:  return .yellow
        case .dark:     return .black
        case .ice:      return .cyan
        case .fairy:    return .pink
        case .dragon:   return .indigo
        case .rock:     return .gray
        case .ghost:    return .indigo
        case .fighting: return .gray
        case .steel:    return .gray
        }
    }
}
