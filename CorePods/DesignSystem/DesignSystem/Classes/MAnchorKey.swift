//
//  MAnchorKey.swift
//  PokeDex
//
//  Created by Ariel Díaz on 30/01/24.
//

import SwiftUI

public struct MAnchorKey: PreferenceKey {
    public static var defaultValue: [String: Anchor<CGRect>] = [:]
    public static func reduce(value: inout [String: Anchor<CGRect>], nextValue: () -> [String: Anchor<CGRect>]) {
        value.merge(nextValue()) {
            $1
        }
    }
}
