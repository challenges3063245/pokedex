//
//  GeometryView.swift
//  DesignSystem
//
//  Created by Ariel Díaz on 01/02/24.
//

import SwiftUI

public struct GeometryView: View {

    public init() { }

    public var body: some View {
        VStack {
            GeometryReader(content: { geometry in
                Color.clear
                    .anchorPreference(key: MAnchorKey.self, value: .bounds, transform: {
                        ["detail": $0]
                    })
            })
            Spacer(minLength: 0)
        }
        .ignoresSafeArea()
    }

}

