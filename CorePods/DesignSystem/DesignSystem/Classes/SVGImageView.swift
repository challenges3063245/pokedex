//
//  SVGImageView.swift
//  DesignSystem
//
//  Created by Ariel Díaz on 27/01/24.
//

import SwiftUI
import SwiftSVG

public struct SVGImageView: UIViewRepresentable {
    let data: Data

    public init(data: Data) {
        self.data = data
    }

    public func makeUIView(context: Context) -> UIView {
        let imageView = UIImageView(SVGData: data)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let view = UIView()
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            imageView.widthAnchor.constraint(equalToConstant: 100),

            view.heightAnchor.constraint(equalToConstant: 100),
            view.widthAnchor.constraint(equalToConstant: 100)
        ])
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    public func updateUIView(_ uiView: UIView, context: Context) { }

}
