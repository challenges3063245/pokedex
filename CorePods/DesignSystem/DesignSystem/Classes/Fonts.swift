//
//  Fonts.swift
//  DesignSystem
//
//  Created by Ariel Díaz on 17/01/24.
//

import Foundation
import SwiftUI

public typealias Completion = () -> Void

extension Font {
    public static var sevenSegmentTitle: Font? {
        .custom("SevenSegmentRegular", size: 32)
    }

    public static var sevenSegmentMediumUp: Font? {
        .custom("SevenSegmentRegular", size: 20)
    }

    public static var sevenSegmentMediumDown: Font? {
        .custom("SevenSegmentRegular", size: 16)
    }

    public static var sevenSegmentSmallUp: Font? {
        .custom("SevenSegmentRegular", size: 12)
    }

    public static var freeTitle: Font? {
        .custom("FreelanceKamchatka", size: 36)
    }

    public static var freeMediumUp: Font? {
        .custom("FreelanceKamchatka", size: 24)
    }

    public static var freeSmallUp: Font? {
        .custom("FreelanceKamchatka", size: 16)
    }
}

extension UIFont {
    public static var freeTitle: UIFont? {
        .init(name: "FreelanceKamchatka", size: 36)
    }

    public static var freeMediumUp: UIFont? {
        .init(name: "FreelanceKamchatka", size: 24)
    }

    public static var freeMediumDown: UIFont? {
        .init(name: "FreelanceKamchatka", size: 20)
    }

    public static var sevenSegmentTitle: UIFont? {
        .init(name: "SevenSegmentRegular", size: 32)
    }

    public static var sevenSegmentMediumUp: UIFont? {
        .init(name: "SevenSegmentRegular", size: 20)
    }

    public static var sevenSegmentMediumDown: UIFont? {
        .init(name: "SevenSegmentRegular", size: 16)
    }
}
