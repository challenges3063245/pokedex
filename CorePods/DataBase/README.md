# DataBase

[![CI Status](https://img.shields.io/travis/Ariel/DataBase.svg?style=flat)](https://travis-ci.org/Ariel/DataBase)
[![Version](https://img.shields.io/cocoapods/v/DataBase.svg?style=flat)](https://cocoapods.org/pods/DataBase)
[![License](https://img.shields.io/cocoapods/l/DataBase.svg?style=flat)](https://cocoapods.org/pods/DataBase)
[![Platform](https://img.shields.io/cocoapods/p/DataBase.svg?style=flat)](https://cocoapods.org/pods/DataBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DataBase is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DataBase'
```

## Author

Ariel, ariel44r@gmail.com

## License

DataBase is available under the MIT license. See the LICENSE file for more info.
