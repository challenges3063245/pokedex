//
//  API.swift
//  Network
//
//  Created by Ariel Díaz on 17/01/24.
//

import Foundation
import Combine

public class NetworkAPI {

    public static let shared: NetworkAPI = NetworkAPI()
    public static let serializedError = NSError(domain: "🚧 \n\nCan not serialize data", code: 500)
    private static let decoder:JSONDecoder = JSONDecoder()
    static let error = NSError(domain: "Internet connection appears to be offline", code: -1009)

    public var subscriptions = Set<AnyCancellable>()

    private init() { }

    public func getSerializedData<T: Decodable>(with request: URLRequest) -> AnyPublisher<T, Error> {
        return self.getData(with: request)
            .receive(on: RunLoop.main)
            .tryMap {
                try NetworkAPI.decoder.decode(T.self, from: $0)
            }
            .eraseToAnyPublisher()
    }

    public func getData(with request: URLRequest) -> Future<Data, Error> {
        debugPrint("[🌐]: fetch data URL: \(request.url?.absoluteString ?? "")")
        return Future() { promise in
            if Reachability.isConnectedToNetwork() {
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
                    if let data {
                        promise(.success(data))
                    } else if let error {
                        promise(.failure(error))
                    }
                }
                task.resume()
            } else {
                promise(.failure(NetworkAPI.error))
            }
        }
    }

    public func downloadImageData(with url: String) -> Future<Data, Error> {
        return Future() { promise in
            if Reachability.isConnectedToNetwork() {
                DispatchQueue.global(qos: .background).async {
                    Cache.shared.downloadContent(fromUrlString: url) { completion in
                        promise(completion)
                    }
                }
            } else {
                promise(.failure(NetworkAPI.error))
            }
        }
    }

}
